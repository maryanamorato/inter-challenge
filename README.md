# Inter Challenge 

Restful CRUD API using Spring Boot, Maven, JPA and H2.

## Getting Started

To run the project, you will need to install the following

* [Java 1.8.x](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
* [Maven 3.6.0](https://maven.apache.org/docs/3.6.0/release-notes.html)

## Installing

```bash
git clone git@gitlab.com:maryanamorato/inter-challenge.git
cd inter-challenge
```

## Building and Running with Maven

```bash
mvn package
java -jar target/interchallenge-0.0.1-SNAPSHOT.jar
```

Alternatively, you can run project without packaging it

```bash
mvn spring-boot:run
```

Good! Now it's running on http://localhost:8080/

## Testing

To run the tests, run the following

```bash
mvn test
```

# Importing into Eclipse/STS

Run the following

```bash
mvn eclipse:eclipse
```

In Eclipse / STS, import the project as an `Existing Maven project`

# Swagger Documentation

Swagger is avaliable on http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/
