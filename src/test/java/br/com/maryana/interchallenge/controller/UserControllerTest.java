package br.com.maryana.interchallenge.controller;

import static br.com.maryana.interchallenge.dto.UserResponseDTO.fromEntityToDTO;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import br.com.maryana.interchallenge.entity.User;
import br.com.maryana.interchallenge.exception.EncryptionException;
import br.com.maryana.interchallenge.exception.NotFoundException;
import br.com.maryana.interchallenge.exception.UnprocessableEntityException;
import br.com.maryana.interchallenge.service.UserService;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {
	@MockBean
	private UserService service;

	@Autowired
	private MockMvc mockMvc;

	@BeforeEach
	void beforeEach() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("POST /user")
	void shouldCreateUser() throws Exception {
		User user = new User("Maryana", "maryanamorato16@gmail.com");
		User userExpected = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, Arrays.asList());
		when(service.create(any())).thenReturn(userExpected);

		mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))

				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is(userExpected.getName())))
				.andExpect(jsonPath("$.email", is(userExpected.getEmail())))
				.andExpect(jsonPath("$.digits", is(userExpected.getDigits())));
	}

	@Test
	@DisplayName("POST /user unprocessable entity")
	void shouldThrowErrorWhenCreateUserWithMissingParams() throws Exception {
		User user = new User("Maryana", null);
		when(service.create(any())).thenThrow(UnprocessableEntityException.class);

		mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	@DisplayName("GET /user/1")
	void shouldReturnUser() throws Exception {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		when(service.find(user.getId())).thenReturn(user);

		mockMvc.perform(get("/user/{id}", user.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is(user.getName())))
				.andExpect(jsonPath("$.email", is(user.getEmail())))
				.andExpect(jsonPath("$.digits", is(user.getDigits())));
	}

	@Test
	@DisplayName("GET /user/1 not found")
	void shouldThrowErrorWhenGetUserByIdNotFound() throws Exception {
		when(service.find(1l)).thenThrow(NotFoundException.class);

		mockMvc.perform(get("/user/{id}", 1l)).andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("GET /user/list")
	void shouldReturnUserList() throws Exception {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		User anotherUser = new User(2l, "Camila", "camilamorato@gmail.com", null, null);

		when(service.list()).thenReturn(Lists.newArrayList(fromEntityToDTO(user), fromEntityToDTO(anotherUser)));

		mockMvc.perform(get("/user/list")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))

				.andExpect(jsonPath("$[0].name", is(user.getName())))
				.andExpect(jsonPath("$[0].email", is(user.getEmail())))
				.andExpect(jsonPath("$[0].digits", is(user.getDigits())))

				.andExpect(jsonPath("$[1].name", is(anotherUser.getName())))
				.andExpect(jsonPath("$[1].email", is(anotherUser.getEmail())))
				.andExpect(jsonPath("$[1].digits", is(anotherUser.getDigits())));
	}

	@Test
	@DisplayName("PUT /user/1")
	void shouldUpdateUser() throws Exception {
		User user = new User("Maryana Morato", "maryanamorato16@gmail.com");
		User userFound = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, Arrays.asList());
		User userUpdated = new User(1l, "Maryana Morato", "maryanamorato16@gmail.com", null, Arrays.asList());

		when(service.find(1l)).thenReturn(userFound);
		when(service.update(eq(1l), any())).thenReturn(userUpdated);

		mockMvc.perform(put("/user/{id}", 1l).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.IF_MATCH, 2)
				.content(asJsonString(user)))

				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is(userUpdated.getName())))
				.andExpect(jsonPath("$.email", is(userUpdated.getEmail())))
				.andExpect(jsonPath("$.digits", is(userUpdated.getDigits())));

	}

	@Test
	@DisplayName("PUT /user/1 unprocessable entity")
	void shouldThrowErrorWhenUpdateUserWithMissingParams() throws Exception {
		User user = new User("Maryana Morato", null);
		User userFound = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, Arrays.asList());

		when(service.find(1l)).thenReturn(userFound);
		when(service.update(eq(1l), any())).thenThrow(UnprocessableEntityException.class);

		mockMvc.perform(put("/user/{id}", 1l).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.IF_MATCH, 2)
				.content(asJsonString(user)))

				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	@DisplayName("DELETE /user/1")
	void shouldDeleteUser() throws Exception {
		mockMvc.perform(delete("/user/{id}", 1l)).andExpect(status().isOk());
	}

	@Test
	@DisplayName("PATCH /user/1")
	void shouldEncryptUser() throws Exception {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, Arrays.asList());
		String mockedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiq0FRQZxTF+wpV7zbMh5U4sGvtf4q/x36WQJOQOD83vSV5VIkgd1zlD9r4LuyPBiPkRHpFvralUNyqk1/Wqnff+MAWRcVrqNQiuIBIYCVOtOsMaAl0GjevtRW5myLAioFiLTtO/WheOlFY6VL7RfHU7Cvj3Dt0REEGGG1wTvKWldEhjahSNrtWqi79lBU6N8u0dMcWrgC2Dz47O789HdZMvjPypGHGOzb+jfJFplFqe8SyvtOXW5uSr4TtKG/i3VMuiiCAJZ/7ybUm/0I1eTOQAjYVkQfvb67yRP21DAB1XGDAFlYYDsMtQSH5w6hfzWrE0yZqgMckkaYTTHg9sOawIDAQAB";

		when(service.encryptUser(eq(1l), any())).thenReturn(new User());
		when(service.find(1l)).thenReturn(user);

		mockMvc.perform(
				patch("/user/encrypt/{id}", 1l).contentType(MediaType.APPLICATION_JSON).content(mockedKey))
				.andExpect(status().isOk());
	}
	
	@Test
	@DisplayName("PATCH /user/1 encryption error")
	void shouldThrowErrorWhenEncryptUserWithInvalidKey() throws Exception {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, Arrays.asList());
		String mockedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AIBCgKCAQEAiq0FRQZxTF+wpV7zbMh5U4sGvtf4q/x36WQJOD83vSV5VIkgd1zlD9r4LuyPBiPkRHpFvralUNyqk1/Wqnff+MAWRcVrqNQiuIBIYCVOtOsMaAl0GjevtRW5myLAioFiLTtO/WheOlFY6VL7RfHU7Cvj3Dt0REEGGG1wTvKWldEhjahSNrtWqi79lBU6N8u0dMcWrgC2Dz47O789HdZMvjPypGHGOzb+jfJFplFqe8SyvtOXW5uSr4TtKG/i3VMuiiCAJZ/7ybUm/0I1eTOQAjYVkQfvb67yRP21DAB1XGDAFlYYDsMtQSH5w6hfzWrE0yZqgMckkaYTTHg9sOawIDAQAB";

		when(service.encryptUser(eq(1l), any())).thenThrow(EncryptionException.class);
		when(service.find(1l)).thenReturn(user);

		mockMvc.perform(
				patch("/user/encrypt/{id}", 1l).contentType(MediaType.APPLICATION_JSON).content(mockedKey))
				.andExpect(status().isInternalServerError());
	}

	static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}