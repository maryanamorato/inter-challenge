package br.com.maryana.interchallenge.controller;

import static br.com.maryana.interchallenge.dto.DigitResponseDTO.fromEntityToDTO;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.exception.BadRequestException;
import br.com.maryana.interchallenge.service.DigitService;
import br.com.maryana.interchallenge.service.UserService;

@SpringBootTest
@AutoConfigureMockMvc
public class DigitControllerTest {
	@MockBean
	private DigitService service;

	@MockBean
	private UserService userService;

	@Autowired
	private MockMvc mockMvc;

	@BeforeEach
	void beforeEach() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("POST /single-digit")
	void shouldCalcultateSingleDigit() throws Exception {
		Digit digit = new Digit("425", 2, 1l);
		Digit digitExpected = new Digit(1l, 1l, "425", 2, 4);

		when(service.calc(any())).thenReturn(digitExpected);

		mockMvc.perform(post("/single-digit").contentType(MediaType.APPLICATION_JSON).content(asJsonString(digit)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$.number", is(digitExpected.getNumber())))
				.andExpect(jsonPath("$.frequency", is(digitExpected.getFrequency())))
				.andExpect(jsonPath("$.singleDigit", is(digitExpected.getSingleDigit())));
	}

	@Test
	@DisplayName("POST /single-digit bad request")
	void shouldThrowErrorWhenCalculateInvalidSingleDigit() throws Exception {
		Digit digit = new Digit("Ops", 2, 1l);

		when(service.calc(any())).thenThrow(BadRequestException.class);

		mockMvc.perform(post("/single-digit").contentType(MediaType.APPLICATION_JSON).content(asJsonString(digit)))
				.andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName("GET /single-digit/user/1")
	void shouldReturnDigitsByUser() throws Exception {
		Digit digit = new Digit(1l, 1l, "425", 2, 4);
		Digit anotherDigit = new Digit(2l, 1l, "23", 1, 5);

		when(service.findDigitsByUser(digit.getUserId()))
				.thenReturn(Arrays.asList(fromEntityToDTO(digit), fromEntityToDTO(anotherDigit)));

		mockMvc.perform(get("/single-digit/user/{id}", digit.getUserId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))

				.andExpect(jsonPath("$[0].number", is(digit.getNumber())))
				.andExpect(jsonPath("$[0].frequency", is(digit.getFrequency())))
				.andExpect(jsonPath("$[0].singleDigit", is(digit.getSingleDigit())))

				.andExpect(jsonPath("$[1].number", is(anotherDigit.getNumber())))
				.andExpect(jsonPath("$[1].frequency", is(anotherDigit.getFrequency())))
				.andExpect(jsonPath("$[1].singleDigit", is(anotherDigit.getSingleDigit())));
	}

	static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
