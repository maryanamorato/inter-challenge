package br.com.maryana.interchallenge.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.Modifying;

import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.entity.User;

@SpringBootTest
@AutoConfigureMockMvc
public class DigitCacheRepositoryTest {
	@Autowired
	private DigitCacheRepository repository;

	@Autowired
	private UserCacheRepository userRepository;

	@Test
	@DisplayName("Test list digits")
	void shouldReturnAllDigits() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		userRepository.save(user);

		Digit digit = new Digit(1l, 1l, "425", 2, 4);
		Digit anotherDigit = new Digit(2l, 1l, "81", 2, 9);

		repository.save(digit);
		repository.save(anotherDigit);

		List<Digit> digits = repository.findDigits();
		Assertions.assertEquals(2, digits.size(), "Expected 2 digits in cache");
	}

	@Test
	@DisplayName("Test list digits by user")
	void shouldReturnDigitsByUser() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		User anotherUser = new User(2l, "Camila", "camilamorato@gmail.com", null, null);

		userRepository.save(user);
		userRepository.save(anotherUser);

		Digit digit = new Digit(1l, 1l, "425", 2, 4);
		Digit anotherDigit = new Digit(2l, 1l, "81", 2, 9);
		Digit oneMoreDigit = new Digit(3l, 2l, "6", 5, 3);

		repository.save(digit);
		repository.save(anotherDigit);
		repository.save(oneMoreDigit);

		List<Digit> digits = repository.findDigitsByUser(user.getId());
		Assertions.assertEquals(2, digits.size(), "Expected 2 digits in cache");
	}

	@Test
	@DisplayName("Test limit digits in cache to 10")
	@Transactional
	void shouldLimitDigitsTo10() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		userRepository.save(user);

		for (long i = 1l; i <= 15; i++) {
			Digit digit = new Digit(i, 1l, Long.toString(i*9), 1, 9);
			repository.save(digit);
		}

		repository.digitLimiter();

		List<Digit> digits = repository.findDigits();
		Assertions.assertEquals(10, digits.size(), "Expected 10 digits in cache");
	}
}
