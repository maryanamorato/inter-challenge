package br.com.maryana.interchallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.maryana.interchallenge.dto.UserResponseDTO;
import br.com.maryana.interchallenge.entity.User;
import br.com.maryana.interchallenge.exception.ConflictException;
import br.com.maryana.interchallenge.exception.EncryptionException;
import br.com.maryana.interchallenge.exception.NotFoundException;
import br.com.maryana.interchallenge.exception.UnprocessableEntityException;
import br.com.maryana.interchallenge.repository.UserCacheRepository;

public class UserServiceTest {
	@InjectMocks
	private UserService service;

	@Mock
	private UserCacheRepository repository;

	@BeforeEach
	void beforeEach() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("Test create user")
	void shouldCreateUser() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		when(repository.save(any())).thenReturn(user);

		User returnedUser = service.create(user);

		verify(repository).save(any());
		assertNotNull(returnedUser, "Saved user should not be null");
	}

	@Test
	@DisplayName("Test create user with missing params")
	void shouldThrowErrorWhenSaveUserWithMissingEmailOrName() {
		User user = new User(1l, "Maryana", null, null, null);
		when(repository.save(any())).thenReturn(user);

		assertThrows(UnprocessableEntityException.class, () -> {
			service.create(user);
		});
	}

	@Test
	@DisplayName("Test find user by ID")
	void shouldFindUser() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		User returnedUser = service.find(user.getId());

		verify(repository).findById(user.getId());
		assertTrue(user != null, "User not found");
		assertSame(returnedUser, user, "User returned was not the same as the mock");
	}

	@Test
	@DisplayName("Test find user by non-existing ID")
	void shouldThrowUserNotFound() {
		when(repository.findById(1l)).thenReturn(Optional.empty());

		assertThrows(NotFoundException.class, () -> {
			service.find(1l);
		});
	}

	@Test
	@DisplayName("Test list users")
	void shouldFindAllUsers() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		User anotherUser = new User(2l, "Camila", "camilamorato@gmail.com", null, null);

		when(repository.findAll()).thenReturn(Arrays.asList(user, anotherUser));
		List<UserResponseDTO> users = service.list();

		verify(repository).findAll();
		assertEquals(2, users.size(), "List should return 2 users");
	}

	@Test
	@DisplayName("Test update user")
	void shouldUpdateUser() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		when(repository.save(user)).thenReturn(user);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		
		service.create(user);
		User returnedUser = service.update(user.getId(), user);

		verify(repository, times(2)).save(any());
		assertNotNull(returnedUser, "Saved user should not be null");
	}
	
	@Test
	@DisplayName("Test update user with missing params")
	void shouldThrowErrorWhenUpdateUserWIthMissingParams() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		User userUpdated = new User("Maryana", null);
	
		when(repository.save(user)).thenReturn(user);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		
		service.create(user);

		assertThrows(UnprocessableEntityException.class, () -> {
			service.update(user.getId(), userUpdated);
		});
	}
	
	@Test
	@DisplayName("Test delete user")
	public void shouldDeleteUser() {
	    User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
	    service.delete(user.getId());

	    verify(repository, times(1)).deleteById(user.getId()); 
	}

	@Test
    @DisplayName("Test add Public Key")
    void shouldReturnEncryptedUser() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		String mockedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiq0FRQZxTF+wpV7zbMh5U4sGvtf4q/x36WQJOQOD83vSV5VIkgd1zlD9r4LuyPBiPkRHpFvralUNyqk1/Wqnff+MAWRcVrqNQiuIBIYCVOtOsMaAl0GjevtRW5myLAioFiLTtO/WheOlFY6VL7RfHU7Cvj3Dt0REEGGG1wTvKWldEhjahSNrtWqi79lBU6N8u0dMcWrgC2Dz47O789HdZMvjPypGHGOzb+jfJFplFqe8SyvtOXW5uSr4TtKG/i3VMuiiCAJZ/7ybUm/0I1eTOQAjYVkQfvb67yRP21DAB1XGDAFlYYDsMtQSH5w6hfzWrE0yZqgMckkaYTTHg9sOawIDAQAB";
	
		when(repository.save(user)).thenReturn(user);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		
		service.create(user);
		User encryptedUser = service.encryptUser(user.getId(), mockedKey);

		verify(repository, times(2)).save(any());
		assertSame(encryptedUser.getUserKey(), mockedKey, "Key returned was not the same as the mock");
		assertNotNull(encryptedUser, "Encrypt user should not be null");
    }
	
	@Test
    @DisplayName("Test add Invalid Public Key")
    void shouldThrowEncryptError() {
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", null, null);
		String mockedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiq0QZxTF+wpV7zbMh5U4sGvtf4q/x36WQJOQOD83vSV5VIkgd1zlD9r4LuyPBiPkRHpFvralUNyqk1/Wqnff+MAWRcVrqNQiuIBIYCVOtOsMaAl0GjevtRW5myLAioFiLTtO/WheOlFY6VL7RfHU7Cvj3Dt0REEGGG1wTvKWldEhjahSNrtWqi79lBU6N8u0dMcWrgC2Dz47O789HdZMvjPypGHGOzb+jfJFplFqe8SyvtOXW5uSr4TtKG/i3VMuiiCAJZ/7ybUm/0I1eTOQAjYVkQfvb67yRP21DAB1XGDAFlYYDsMtQSH5w6hfzWrE0yZqgMckkaYTTHg9sOawIDAQAB";
	
		when(repository.save(user)).thenReturn(user);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		
		service.create(user);

		assertThrows(EncryptionException.class, () -> {
			service.encryptUser(user.getId(), mockedKey);
		});
    }
	
	@Test
    @DisplayName("Test add Public Key in Already Encrypted User")
    void shouldThrowErrorDueToAlreadyEncryptedUser() {
		String mockedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiq0FRQZxTF+wpV7zbMh5U4sGvtf4q/x36WQJOQOD83vSV5VIkgd1zlD9r4LuyPBiPkRHpFvralUNyqk1/Wqnff+MAWRcVrqNQiuIBIYCVOtOsMaAl0GjevtRW5myLAioFiLTtO/WheOlFY6VL7RfHU7Cvj3Dt0REEGGG1wTvKWldEhjahSNrtWqi79lBU6N8u0dMcWrgC2Dz47O789HdZMvjPypGHGOzb+jfJFplFqe8SyvtOXW5uSr4TtKG/i3VMuiiCAJZ/7ybUm/0I1eTOQAjYVkQfvb67yRP21DAB1XGDAFlYYDsMtQSH5w6hfzWrE0yZqgMckkaYTTHg9sOawIDAQAB";
		User user = new User(1l, "Maryana", "maryanamorato16@gmail.com", mockedKey, null);
	
		when(repository.save(user)).thenReturn(user);
		when(repository.findById(user.getId())).thenReturn(Optional.of(user));
		
		service.create(user);

		assertThrows(ConflictException.class, () -> {
			service.encryptUser(user.getId(), mockedKey);
		});
    }
}