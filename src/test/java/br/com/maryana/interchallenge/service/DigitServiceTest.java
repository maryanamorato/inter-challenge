package br.com.maryana.interchallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.maryana.interchallenge.dto.DigitResponseDTO;
import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.exception.BadRequestException;
import br.com.maryana.interchallenge.repository.DigitCacheRepository;

public class DigitServiceTest {
    @InjectMocks
    private DigitService service;

    @Mock
    private DigitCacheRepository digitRepository;
    
    @BeforeEach
	void beforeEach() {
		MockitoAnnotations.initMocks(this);
	}
    
    @Test
    @DisplayName("Test get single digit")
    void shouldReturnSingleDigit() {
        Digit digit = new Digit(1l, 1l, "425", 2, 4);
        when(digitRepository.save(any())).thenReturn(digit);

        Digit returnedDigit = service.calc(new Digit("425", 2, 1l));

        verify(digitRepository).save(any());
        assertSame(returnedDigit, digit, "Digit returned was not the same as the mock");
    }
    
    @Test
    @DisplayName("Test get single digit passing string in number field")
    void shouldThrowErrorPassingStringInSingleDigit() {
        Digit digit = new Digit(1l, 1l, "Ops", 2, 4);
        when(digitRepository.save(any())).thenReturn(digit);

        assertThrows(BadRequestException.class, () -> {
            service.calc(digit);
          });
    }
    
    @Test
    @DisplayName("Test list digits by user ID")
    void shouldReturnDigitList() {
        Digit digit = new Digit(1l, 1l, "425", 2, 4);
        Digit anotherDigit = new Digit(2l, 1l, "23", 1, 5);

        when(digitRepository.findDigitsByUser(1l)).thenReturn(Arrays.asList(digit, anotherDigit));

        List<DigitResponseDTO> digits = service.findDigitsByUser(1l);

        verify(digitRepository).findDigitsByUser(1l);
        assertEquals(2, digits.size(), "List should return 2 digits");
    }
}
