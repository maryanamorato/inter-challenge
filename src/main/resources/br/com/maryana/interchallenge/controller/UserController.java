package br.com.maryana.interchallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maryana.interchallenge.dto.DigitResponseDTO;
import br.com.maryana.interchallenge.dto.UserRequestDTO;
import br.com.maryana.interchallenge.dto.UserResponseDTO;
import br.com.maryana.interchallenge.entity.User;
import br.com.maryana.interchallenge.service.UserService;

@Configuration
@RestController
@RequestMapping(value = "/user")

public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<?> create(@RequestBody UserRequestDTO user) {
		try {
			User userCreated = userService.create(user.fromDTOToEntity());
			

			if (userCreated == null) {
				return new ResponseEntity<String>("Please provide your email and name",
						HttpStatus.UNPROCESSABLE_ENTITY);
			}

			return new ResponseEntity<UserResponseDTO>(UserResponseDTO.fromEntityToDTO(userCreated), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") long id) {

		if (userService.find(id) == null) {
			return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<UserResponseDTO>(UserResponseDTO.fromEntityToDTO(userService.find(id)),
				HttpStatus.OK);
	}

	@PutMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody UserRequestDTO user) {
		try {
			User updatedUser = userService.update(id, user.fromDTOToEntity());
			
			if (updatedUser == null) {
				return new ResponseEntity<String>("Please provide your email, name and your Public RSA Key (2048 bits)",
						HttpStatus.UNPROCESSABLE_ENTITY);
			}
			
			return new ResponseEntity<UserResponseDTO>(UserResponseDTO.fromEntityToDTO(updatedUser), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<String> delete(@PathVariable("id") long id) {
		try {
			userService.delete(id);
			return new ResponseEntity<>("Successfully deleted", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/list", produces = "application/json")
	public ResponseEntity<?> list() {
		return new ResponseEntity<List<UserResponseDTO>>(userService.list(), HttpStatus.OK);
	}

	@GetMapping(path = "/{id}/digits", produces = "application/json")
	public ResponseEntity<?> findDigitsByUser(@PathVariable("id") long id) {

		if (userService.findDigits(id).isEmpty()) {
			return new ResponseEntity<String>("User/digits not found", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<DigitResponseDTO>>(userService.findDigits(id), HttpStatus.OK);
	}
}
