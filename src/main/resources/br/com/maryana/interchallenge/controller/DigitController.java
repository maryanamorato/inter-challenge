package br.com.maryana.interchallenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maryana.interchallenge.dto.DigitRequestDTO;
import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.service.DigitService;
import br.com.maryana.interchallenge.service.UserService;

@Configuration
@RestController
@RequestMapping(value = "/unique-digit")

public class DigitController {

	@Autowired
	private DigitService digitService;

	@Autowired
	private UserService userService;

	@PostMapping(produces = "application/json")
	public ResponseEntity<?> calc(@RequestBody DigitRequestDTO digit) {
		try {

			if (userService.find(digit.getUserId()) == null) {
				return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
			}

			digitService.limiter();
			Digit result = digitService.calc(digit.fromDTOToEntity());
			return new ResponseEntity<Digit>(result, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
