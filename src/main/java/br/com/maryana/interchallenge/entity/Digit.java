package br.com.maryana.interchallenge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data

@Entity
@Table(name = "DIGIT")
public class Digit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "ID")
	private Long id;

	@NotNull(message = "You must provide your user ID")
	@Column(name = "USER_ID")
	private Long userId;

	@NotNull(message = "You must provide a number")
	@Column(name = "NUMBER")
	private String number;

	@NotNull(message = "You must provide the number frequency")
	@Column(name = "FREQUENCY")
	private Integer frequency;

	@Column(name = "SINGLE_DIGIT")
	private Integer singleDigit;

	public Digit(String number, Integer frequency, Long userId) {
		// Constructor to be used in Requests DTOs
		this.number = number;
		this.frequency = frequency;
		this.userId = userId;
	}
}
