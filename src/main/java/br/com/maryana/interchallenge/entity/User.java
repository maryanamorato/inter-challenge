package br.com.maryana.interchallenge.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "USERS")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;

	@NotNull(message = "You must provide your name")
	@Size(min = 2, message = "Name must have at least 2 characters")
	@Column(name = "NAME")
	private String name;

	@NotNull(message = "You must provide your email")
	@Column(name = "EMAIL")
	private String email;

	@Column(name = "USER_KEY")
	private String userKey;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private List<Digit> digits;

	public User(String name, String email) {
		// Constructor to be used in Requests DTOs
		this.name = name;
		this.email = email;
	}
}
