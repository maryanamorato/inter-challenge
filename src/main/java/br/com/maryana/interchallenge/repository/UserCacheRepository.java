package br.com.maryana.interchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maryana.interchallenge.entity.User;

public interface UserCacheRepository extends JpaRepository<User, Long> {
}
