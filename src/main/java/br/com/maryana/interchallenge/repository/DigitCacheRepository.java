package br.com.maryana.interchallenge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.maryana.interchallenge.entity.Digit;

public interface DigitCacheRepository extends JpaRepository<Digit, Long> {
	@Query(value = "SELECT * FROM DIGIT", nativeQuery = true)
	List<Digit> findDigits();

	@Query(value = "SELECT D.* FROM DIGIT AS D WHERE D.USER_ID = :userId", nativeQuery = true)
	List<Digit> findDigitsByUser(@Param("userId") long userId);

	@Modifying
	@Query(value = "DELETE FROM DIGIT WHERE ID NOT IN (SELECT ID FROM DIGIT ORDER BY ID DESC LIMIT 10)", nativeQuery = true)
	void digitLimiter();
}