package br.com.maryana.interchallenge.service;

import static java.util.Objects.nonNull;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maryana.interchallenge.dto.UserResponseDTO;
import br.com.maryana.interchallenge.entity.User;
import br.com.maryana.interchallenge.exception.ConflictException;
import br.com.maryana.interchallenge.exception.EncryptionException;
import br.com.maryana.interchallenge.exception.NotFoundException;
import br.com.maryana.interchallenge.exception.UnprocessableEntityException;
import br.com.maryana.interchallenge.repository.UserCacheRepository;

@Service

public class UserService {
	@Autowired
	private UserCacheRepository repository;

	private boolean isFilled(String field) {
		return field != null && !field.isEmpty();
	}

	private static String encrypt(String text, String key) {
		byte[] keyBytes = Base64.getDecoder().decode(key.replace("\n", ""));
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);

		KeyFactory keyFactory;

		try {
			keyFactory = KeyFactory.getInstance("RSA");
			PublicKey newKey = keyFactory.generatePublic(spec);

			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, newKey);

			return DatatypeConverter.printBase64Binary(cipher.doFinal(text.getBytes()));
		} catch (Exception e) {
			throw new EncryptionException("Encrypt error: " + e.getMessage());
		}

	}

	public User create(User user) {

		if (isFilled(user.getEmail()) && isFilled(user.getName())) {
			user.setEmail(user.getEmail());
			user.setName(user.getName());

			return repository.save(user);
		}

		throw new UnprocessableEntityException("Please provide your email and name");
	}

	public User find(long id) {
		Optional<User> user = repository.findById(id);

		if (!user.isPresent()) {
			throw new NotFoundException("User not found");
		}

		return user.get();
	}

	public User update(long id, User newUser) {
		User user = find(id);

		if (isFilled(newUser.getEmail()) && isFilled(newUser.getName())) {

			if (isFilled(user.getUserKey())) {
				user.setEmail(encrypt(newUser.getEmail(), user.getUserKey()));
				user.setName(encrypt(newUser.getName(), user.getUserKey()));
			} else {
				user.setEmail(newUser.getEmail());
				user.setName(newUser.getName());
			}

			return repository.save(user);
		}

		throw new UnprocessableEntityException("Please provide your email and name");
	}

	public User encryptUser(long id, String key) {
		User user = find(id);

		if (isFilled(user.getUserKey())) {
			throw new ConflictException("User already encrypted");
		}

		user.setEmail(encrypt(user.getEmail(), key));
		user.setName(encrypt(user.getName(), key));
		user.setUserKey(key);

		return repository.save(user);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public List<UserResponseDTO> list() {
		List<User> users = repository.findAll();
		List<UserResponseDTO> userDTOs = new ArrayList<>();

		if (nonNull(users) && !users.isEmpty()) {
			for (User user : users) {
				UserResponseDTO dto = UserResponseDTO.fromEntityToDTO(user);
				userDTOs.add(dto);
			}
		}

		return userDTOs;
	}
}
