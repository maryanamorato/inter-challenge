package br.com.maryana.interchallenge.service;

import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.maryana.interchallenge.dto.DigitResponseDTO;
import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.exception.BadRequestException;
import br.com.maryana.interchallenge.repository.DigitCacheRepository;

@Service

public class DigitService {

	@Autowired
	private DigitCacheRepository repository;

	@Transactional
	public void limiter() {
		repository.digitLimiter();
	}

	public Digit calc(Digit digit) {
		char[] chars = String.join("", Collections.nCopies(digit.getFrequency(), digit.getNumber())).toCharArray();
		int sum = 0;

		if (Pattern.matches("[a-zA-Z]+", digit.getNumber())) {
			throw new BadRequestException("Invalid number entry");
		}

		List<Digit> existentDigits = repository.findDigits();

		for (Digit existentDigit : existentDigits) {

			if (existentDigit.getNumber().equals(digit.getNumber())
					&& existentDigit.getFrequency().equals(digit.getFrequency())) {
				digit.setSingleDigit(existentDigit.getSingleDigit());
				return repository.save(digit);
			}
		}

		while (chars.length > 1) {
			sum = 0;

			for (int i = 0; i < chars.length; i++) {
				sum += Character.getNumericValue(chars[i]);
			}

			chars = Integer.toString(sum).toCharArray();

		}

		digit.setSingleDigit((int) sum);
		return repository.save(digit);
	}
	
	public List<DigitResponseDTO> findDigitsByUser(long id) {
		List<Digit> digits = repository.findDigitsByUser(id);
		List<DigitResponseDTO> digitDTOs = new ArrayList<>();

		if (nonNull(digits) && !digits.isEmpty()) {
			for (Digit digit : digits) {
				DigitResponseDTO dto = DigitResponseDTO.fromEntityToDTO(digit);
				digitDTOs.add(dto);
			}
		}

		return digitDTOs;
	}
}
