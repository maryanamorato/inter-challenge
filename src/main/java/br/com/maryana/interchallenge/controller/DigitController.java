package br.com.maryana.interchallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maryana.interchallenge.dto.DigitRequestDTO;
import br.com.maryana.interchallenge.dto.DigitResponseDTO;
import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.exception.BadRequestException;
import br.com.maryana.interchallenge.exception.NotFoundException;
import br.com.maryana.interchallenge.service.DigitService;
import br.com.maryana.interchallenge.service.UserService;

@Configuration
@RestController
@RequestMapping(value = "/single-digit")

public class DigitController {

	@Autowired
	private DigitService digitService;

	@Autowired
	private UserService userService;

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<DigitResponseDTO> calc(@RequestBody DigitRequestDTO digit) {
		try {
			userService.find(digit.getUserId());
			digitService.limiter();
			Digit result = digitService.calc(digit.fromDTOToEntity());

			return new ResponseEntity<>(DigitResponseDTO.fromEntityToDTO(result), HttpStatus.OK);

		} catch (BadRequestException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(path = "/user/{id}", produces = "application/json")
	public ResponseEntity<List<DigitResponseDTO>> findDigitsByUser(@PathVariable("id") long id) {
		try {
			userService.find(id);
			return new ResponseEntity<>(digitService.findDigitsByUser(id), HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);

		}
	}
}
