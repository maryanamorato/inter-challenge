package br.com.maryana.interchallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maryana.interchallenge.dto.UserRequestDTO;
import br.com.maryana.interchallenge.dto.UserResponseDTO;
import br.com.maryana.interchallenge.entity.User;
import br.com.maryana.interchallenge.exception.ConflictException;
import br.com.maryana.interchallenge.exception.EncryptionException;
import br.com.maryana.interchallenge.exception.NotFoundException;
import br.com.maryana.interchallenge.exception.UnprocessableEntityException;
import br.com.maryana.interchallenge.service.UserService;

@Configuration
@RestController
@RequestMapping(value = "/user")

public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<UserResponseDTO> create(@RequestBody UserRequestDTO user) {
		try {
			User userCreated = userService.create(user.fromDTOToEntity());
			return new ResponseEntity<>(UserResponseDTO.fromEntityToDTO(userCreated), HttpStatus.OK);
		} catch (UnprocessableEntityException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public ResponseEntity<UserResponseDTO> find(@PathVariable("id") long id) {
		try {
			User userReturned = userService.find(id);
			return new ResponseEntity<>(UserResponseDTO.fromEntityToDTO(userReturned), HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(path = "/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<UserResponseDTO> update(@PathVariable("id") long id, @RequestBody UserRequestDTO user) {
		try {
			User updatedUser = userService.update(id, user.fromDTOToEntity());
			return new ResponseEntity<>(UserResponseDTO.fromEntityToDTO(updatedUser), HttpStatus.OK);
		} catch (UnprocessableEntityException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PatchMapping(path = "encrypt/{id}", produces = "application/json")
	public ResponseEntity<UserResponseDTO> encrypt(@PathVariable("id") long id, @RequestBody String key) {
		try {
			User encryptedUser = userService.encryptUser(id, key);
			return new ResponseEntity<>(UserResponseDTO.fromEntityToDTO(encryptedUser), HttpStatus.OK);
		} catch (ConflictException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.CONFLICT);
		} catch (EncryptionException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") long id) {
		userService.delete(id);
		return new ResponseEntity<>("Successfully deleted", HttpStatus.OK);
	}

	@GetMapping(path = "/list", produces = "application/json")
	public ResponseEntity<List<UserResponseDTO>> list() {
		return new ResponseEntity<>(userService.list(), HttpStatus.OK);
	}
}
