package br.com.maryana.interchallenge.exception;

public class UnprocessableEntityException extends RuntimeException {
	public UnprocessableEntityException (String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
