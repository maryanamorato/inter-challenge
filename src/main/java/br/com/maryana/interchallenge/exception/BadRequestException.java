package br.com.maryana.interchallenge.exception;

public class BadRequestException extends RuntimeException {
	public BadRequestException (String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
