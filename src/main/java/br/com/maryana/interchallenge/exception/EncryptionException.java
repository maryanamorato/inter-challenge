package br.com.maryana.interchallenge.exception;

public class EncryptionException extends RuntimeException {
	public EncryptionException (String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
