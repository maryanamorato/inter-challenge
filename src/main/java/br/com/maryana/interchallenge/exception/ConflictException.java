package br.com.maryana.interchallenge.exception;

public class ConflictException extends RuntimeException {
	public ConflictException (String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
