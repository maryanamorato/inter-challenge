package br.com.maryana.interchallenge.dto;

import br.com.maryana.interchallenge.entity.Digit;
import lombok.Data;

@Data
public class DigitResponseDTO {

	private String number;
	private Integer frequency;
	private Integer singleDigit;

	public static DigitResponseDTO fromEntityToDTO(Digit digit) {
		DigitResponseDTO response = new DigitResponseDTO();

		response.setNumber(digit.getNumber());
		response.setFrequency(digit.getFrequency());
		response.setSingleDigit(digit.getSingleDigit());

		return response;
	}

}
