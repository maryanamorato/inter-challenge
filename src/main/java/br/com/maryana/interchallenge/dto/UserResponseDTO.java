package br.com.maryana.interchallenge.dto;

import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.maryana.interchallenge.entity.Digit;
import br.com.maryana.interchallenge.entity.User;
import lombok.Data;

@Data
public class UserResponseDTO {
	private long id;
	private String name;
	private String email;
	private List<DigitResponseDTO> digits;

	public static UserResponseDTO fromEntityToDTO(User user) {
		UserResponseDTO response = new UserResponseDTO();

		response.setId(user.getId());
		response.setName(user.getName());
		response.setEmail(user.getEmail());

		if (nonNull(user.getDigits())) {
			List<DigitResponseDTO> digitDTOs = new ArrayList<>();

			for (Digit digit : user.getDigits()) {
				DigitResponseDTO dto = DigitResponseDTO.fromEntityToDTO(digit);
				digitDTOs.add(dto);
			}

			response.setDigits(digitDTOs);
		}

		return response;
	}
}
