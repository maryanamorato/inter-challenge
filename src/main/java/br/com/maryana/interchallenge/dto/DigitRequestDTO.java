package br.com.maryana.interchallenge.dto;

import br.com.maryana.interchallenge.entity.Digit;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DigitRequestDTO {

	private String number;
	private String frequency;
	private long userId;

	public Digit fromDTOToEntity() {
		return new Digit(number, Integer.parseInt(frequency), userId);
	}

}