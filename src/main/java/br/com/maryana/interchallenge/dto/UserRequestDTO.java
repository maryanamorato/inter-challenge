package br.com.maryana.interchallenge.dto;

import br.com.maryana.interchallenge.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRequestDTO {
	private String name;
	private String email;

	public User fromDTOToEntity() {
		return new User(name, email);
	}

}
